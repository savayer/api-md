## API

Returns 1 if it's okay:  


***/api/add_day_absent*** - post query to adding absents of a day for students' table by classes  

- studentId *(required)* - integer  
- date *(optional)* - Y-m-d  



***/api/add_subj_absent*** - by subjects  

- studentId *(required)* - integer  
- subjectId *(required)* - integer  
- date *(optional)* - Y-m-d  



***/api/add_day_delay*** - post query to adding delays for students' table by classes  

- studentId *(required)* - integer  
- date *(optional)* - Y-m-d  



***/api/add_subj_delay*** - by subjects  
  
- studentId *(required)* - integer  
- subjectId *(required)* - integer  
- date *(optional)* - Y-m-d  


Returns json data if it's okay:  

***/api/check_student_pswd*** - post query to check a student password  

- phone *(required)*  
- password *(required)*  

and the same with:

***/api/check_teacher_pswd*** - check teacher password  

### Getting data

***/api/get_classes*** - get all classes  

***/api/get_subjects*** - get all subjects  

***/api/get_classes_by_coordinator/{coordinator_id}*** - a get query to recieve classes that attach to {coordinator_id}


***/api/get_students_by_mentor/{mentor_id}*** - a get query to recieve students that attach to {mentor_id}


***/api/get_subjects_by_teacher/{teacher_id}*** - a get query to recieve subjects that attach to {teacher_id}  

***/api/get_students_by_subject/{subject_id}***  

***/api/get_students_by_class/{class_id}***  

### Setting Data

***/api/set_student_arrival_datetime*** - a post query to set arrival time of a student  
***/api/set_student_leaving_datetime*** - a post query to set leaving time of a student  

- studentId *(required)* - integer
- date *(optional)* - Y-m-d H:i:s  

***/api/sync_subjects_in_teacher*** - the method allows to synchronize the IDs array of subjects for teacher  

- teacherId *(required)* - integer
- subjectsArray *(required)* - [1,2,3]  

***/api/sync_students_in_mentors*** - the method allows to synchronize the IDs array of students for mentor  

- mentorId *(required)* - integer
- studentsArray *(required)* - [1,2,3]  

***/api/sync_classes_in_coordinators*** - the method allows to synchronize the IDs array of classes for coordinator  

- coordinatorId *(required)* - integer
- classesArray *(required)* - [1,2,3]  

Returns 1 if it's okay.

### Send A Report

***/api/create_report***  

- date_start *(required)* - Y-m-d
- date_end *(required)* - Y-m-d
- type *(required)* - **class** or **subject**
- id *(required)* - class_id or subject_id
- email *(required)* - email of a reciever  